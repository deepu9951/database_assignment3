import sqlite3
from prettytable import from_db_cursor

con3 = sqlite3.connect('database.sqlite')
cursor3 = con3.cursor()
cursor3.execute("SELECT HomeTeam,FTHG,FTAG FROM Matches WHERE Season = 2010 AND HomeTeam='Aachen' ORDER BY FTHG DESC ")
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)


cursor3.execute("SELECT HomeTeam,COUNT(FTR) FROM Matches WHERE Season = 2016 AND FTR = 'H' GROUP BY HomeTeam ORDER BY COUNT(FTR) DESC ")
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)

cursor3.execute("SELECT * FROM Unique_Teams LIMIT 10 ")
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)

cursor3.execute("SELECT Teams_In_Matches.Match_ID, Teams_In_Matches.Unique_Team_ID, Unique_Teams.TeamName FROM Teams_In_Matches INNER JOIN Unique_Teams ON Unique_Teams.Unique_Team_ID = Teams_In_Matches.Unique_Team_ID  ")
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)

cursor3.execute("SELECT Unique_Teams.Unique_Team_ID,Teams.TeamName,Teams.Season,Teams.KaderHome,Teams.AvgAgeHome,Teams.ForeignPlayersHome,Teams.OverallMarketValueHome,Teams.AvgMarketValueHome,Teams.StadiumCapacity FROM Teams INNER JOIN Unique_Teams ON Unique_Teams.TeamName = Teams.TeamName LIMIT 10 ")
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)

cursor3.execute("SELECT Unique_Teams.Unique_Team_ID,Unique_Teams.TeamName,Teams.Season,Teams.AvgAgeHome,Teams.ForeignPlayersHome FROM Teams INNER JOIN Unique_Teams ON Unique_Teams.TeamName = Teams.TeamName LIMIT 5 ")
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)

char = 'r'
query_c = 'SELECT Teams_In_Matches.Unique_Team_ID,Unique_Teams.TeamName,MAX(Teams_In_Matches.Match_ID) FROM Teams_In_Matches INNER JOIN Unique_Teams ON Unique_Teams.Unique_Team_ID = Teams_In_Matches.Unique_Team_ID WHERE (Unique_Teams.TeamName LIKE "%y") OR (Unique_Teams.TeamName LIKE "%{0}") GROUP BY Unique_Teams.TeamName  '.format(char)
cursor3.execute(query_c)
record = from_db_cursor(cursor3)
print(record)
print('\n'*5)
