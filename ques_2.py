import sqlite3

con = sqlite3.connect('database.sqlite')
cursor2 = con.cursor()
cursor2.execute("SELECT COUNT(*) FROM Teams")
rows = cursor2.fetchall()
print(str(rows))
print('\n'*5)

cursor2.execute("SELECT DISTINCT(Season) FROM Teams ")
seasons = cursor2.fetchall()
print(seasons)
print('\n'*5)

cursor2.execute("SELECT MAX(StadiumCapacity),MIN(StadiumCapacity) FROM Teams ")
mini = cursor2.fetchall()
print('Maximum Stadium Capacity: {0}'.format(mini[0][0]))
print('Minimum Stadium Capacity: {0}'.format(mini[0][1]))
print('\n'*5)

cursor2.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season = 2014")
squad_sum = cursor2.fetchall()
print(squad_sum)
print('\n'*5)


cursor2.execute("SELECT ROUND(AVG(FTHG),2) FROM Matches WHERE HomeTeam = 'Man United'")
goal_avg = cursor2.fetchall()
print(goal_avg)
print('\n'*5)