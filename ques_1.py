import sqlite3
from prettytable import from_db_cursor

conn = sqlite3.connect('database.sqlite')
cursor = conn.cursor()
query_b = "SELECT HomeTeam,AwayTeam FROM Matches WHERE Season = 2015 AND FTHG = 5"
cursor.execute(query_b)
record = from_db_cursor(cursor)
print(record)
print('\n'*5)

query_c = "SELECT * FROM Matches WHERE HomeTeam = 'Arsenal' AND FTR = 'A'" 
cursor.execute(query_c)
record = from_db_cursor(cursor)
print(record)
print('\n'*5)

query_d = "SELECT * FROM Matches WHERE AwayTeam = 'Bayern Munich' AND FTAG > 2 AND Date BETWEEN 2012 AND 2016" 
cursor.execute(query_d)
record = from_db_cursor(cursor)
print(record)
print('\n'*5)

query_e = "SELECT * FROM Matches WHERE AwayTeam LIKE 'M%' AND HomeTeam LIKE 'A%'"
cursor.execute(query_e)
record = from_db_cursor(cursor)
print(record)
print('\n'*5)

conn.commit()
conn.close()
